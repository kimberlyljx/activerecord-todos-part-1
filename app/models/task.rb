class Task < ActiveRecord::Base

  def self.list_all(print)
    list = []
    n = 1
    Task.all.select do |x|
      list << x.id
      puts "#{n.to_s}." + " #{x.title}" if print
      n += 1
    end
    return list
  end

  def self.add(input)
    task = self.create(title: input)
    puts "#{input} was added"
  end

  def self.delete(input)
    list = list_all(false)
    if (list[input.to_i - 1].nil?) || input.to_i <= 0
      puts "Please try again!"
    else
    task = Task.find_by(id: list_all(false)[input.to_i - 1])
    puts "#{task.title} was destroyed!"
    task.destroy
    end
  end

  def self.complete(input)
    list = list_all(false)
    if (list[input.to_i - 1].nil?) || input.to_i <= 0
      puts "Please try again!"
    else
    task = Task.find_by(id: list_all(false)[input.to_i - 1])
    puts "#{task.title} was completed!"
    temp = task.title
    task.update(title: 'Completed ' + temp)
    end
  end

end

