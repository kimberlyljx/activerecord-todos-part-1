require_relative 'config/application'
require_relative 'app/models/task'
require 'byebug'
require 'sqlite3'

# puts "Put your application code in #{File.expand_path(__FILE__)}"

   command = ARGV.first
    input = ARGV.last(ARGV.length-1)
    case command
    when 'list'
      Task.list_all(true)
    when 'add'
      Task.add(input.join(" "))
    when 'delete'
      Task.delete(input[0])
    when 'complete'
      Task.complete(input[0])
    end
