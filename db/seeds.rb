require 'faker'
require_relative '../app/models/task'

10.times do
Task.create(title: Faker::Lorem.sentence)
end